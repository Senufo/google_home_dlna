# Google HOME DLNA

[![Generic badge](https://img.shields.io/badge/code-python-lightgrey.svg)](https://shields.io/)

[![Generic badge](https://img.shields.io/badge/Maintener-Senufo-lightgrey.svg)](https://framagit.org/Senufo)

![Generic badge](https://img.shields.io/badge/Raspbian_GNU_Linux-9-orange.svg)

![Generic badge](https://img.shields.io/badge/Archlinux-blue.svg)

Play mp3 files from a playlist on your Google Home or Chromecast.

You need to rename *settings-exeample.json* in *settings.json* and customize it.

You need to create a Dialogflow Agent
