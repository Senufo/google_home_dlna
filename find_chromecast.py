#!/usr/bin/python3
"""
Find chromecast on network
"""
import time
import pychromecast
from pathlib import Path

global mc
global cast

#cast = pychromecast.Chromecast(ip_chromecast)
casts = pychromecast.get_chromecasts()
if len(casts) == 0:
  print("No Devices Found")
  exit()
for cast in casts:
#  print(cast.__dict__) # liste les méthode de cast
  print("Cast friendly name : {}, IP {} ".format(cast.device.friendly_name, cast.host))
  print(cast.device)
  time.sleep(1)
#  print(cast.status)
#  print(cast.media_controller.status)
#  print("IP : {}".format(cast.host))
#  if cast.status.app_id:
#    print("Killing existing app... %s" % cast.status.app_id)
#    cast.quit_app()
#    time.sleep(5)
#    mc = cast.media_controller
#    return (mc, cast)

