#!/usr/bin/python3
"""
FLASK server that listens to DIALOGFLOW instructions to start an
MP3 playlist on a GOOGLE HOME or CHROMECAST
Version : 0.3.0
"""
import logging
import json
import pychromecast
import subprocess

from song import song
from flask import Flask, request, render_template, jsonify, make_response

# Load settings
f = open('settings.json', 'r')
content = f.read()
f.close()

# Get default settings variable
settings = json.loads(content)

app = Flask(__name__)

def json_reponse(msg, UserResponse='false'):
    '''
    Make JSON reponse for Google Assistant
    msg : text to speech on Google Home
    '''
    res = { "payload": {"google": {
                        "expectUserResponse": "true",
                        "richResponse": {"items": [
                            {"simpleResponse": {
                                "textToSpeech": "TEST"}
                                }
                                ]
                         }
                        }
                        }
        }
    res['payload']['google']['expectUserResponse'] = 'false'
    res['payload']['google']['richResponse']['items'][0]['simpleResponse']['textToSpeech'] = msg
    return res

@app.before_request
def validate_channel_key():
    """
    Verify Token Key header is present on each request.
    and that its value matches the channel key we got from DIALOGFLOW.
    If a request fails this check, exit.
    """
    logger = logging.getLogger('[Google_DLNA]') #.setLevel(logging.DEBUG)
    logger.info("Validate_channel_key")

    try:
        for item in request.json:
            logger.info("Item[{}] : {}".format(item,request.json[item]))
    except TypeError as ex:
        logger.info('Erreur NoneType: {}'.format(ex))
    # Verify token (control access for Google Assistant)
    try:
        token = request.headers['Token'] #for google action
        if token == settings['token']:
            logger.info("TOKEN OK")
            return
        else:
            logger.info("Bad token EXIT")
            exit()
    except Exception as ex:
        logger.info("No token exit !! ({})".format(ex))
        exit()
    return

@app.route('/webhook/', methods=['POST'])
def webhook():
    '''
    The webhook for Google Assistant: 1 action
    * lance_playlist : run playlist
    '''
    logger = logging.getLogger('[Google_DLNA]')
    logger.info('========================WebHook flow=========================')
    liste_chromecasts = []
    # Get devices list
    for key in settings['chromecasts'].items():
        liste_chromecasts.append(key[0].upper())
    for item in request.json:
      logger.info("-> {} json: {}<-".format(item,request.json[item]))
      logger.info('ACTION : >{}<'.format(request.json['queryResult']['action']))
      if request.json['queryResult']['action'] == 'lance_playlist':
        logger.info('APPAREIL : >{}<'.format(request.json['queryResult']['parameters']['appareil']))
        playlist = request.json['queryResult']['parameters']['playlist']
        if (request.json['queryResult']['parameters']['appareil'].upper() in liste_chromecasts):
            subprocess.Popen(["python3", "lance_playlist.py", 
                "-p {}".format(playlist[0]),
                "-c {}".format(request.json['queryResult']['parameters']['appareil'].upper())])
        else:
            subprocess.Popen(["python3", "lance_playlist.py", "-p {}".format(playlist[0])])
        logger.info("La playlist {} est lancée".format(playlist[0]))
        res = json_reponse("La playlist {} est lancée".format(playlist[0]))
        return make_response(jsonify(res))

# create logger
logger = logging.getLogger('[Google_DLNA]')
logging.basicConfig()
# set default logging level
if settings['loggingLevel'] == 'DEBUG':
    logger.setLevel(logging.DEBUG)
    logger.info("Level DEBUG")
if settings['loggingLevel'] == 'INFO':
    logger.setLevel(logging.INFO)
    logger.info("Level INFO")
if settings['loggingLevel'] == 'WARNING':
    logger.setLevel(logging.WARNING)
    logger.info("Level WARNING")
if settings['loggingLevel'] == 'NOTSET':
    logger.setLevel(logging.NOTSET)
    logger.info("Level NOTSET")

if __name__ == '__main__':
    app.run(debug=settings['debug'],
            host=settings['flask_ip'],
            port=int(settings['flask_port']))
