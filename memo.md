
# Mémo flask et dialogflow

- [Mémo flask et dialogflow](#m%C3%A9mo-flask-et-dialogflow)
  - [Notes](#notes)
  - [Ecriture de DIALOGFLOW](#ecriture-de-dialogflow)
    - [Intents](#intents)
      - [*playlist*](#playlist)
      - [*playlist sur appareil*](#playlist-sur-appareil)
    - [Entities](#entities)
    - [Fulfillment](#fulfillment)

## Notes

- Pour éviter une boucle mettre dans dialogflow _set this intent as end of conversation_
- Penser à changer de version test à chaque modification dans dialogflow
- DialogFlow nécesite une réponse en JSON
- Attention la connection en https ne fonctionne pas si le certificat n'est pas valide
- Penser à renouveller le certificat let's encrypt [^1]

Définition de play_media :

```python
def play_media(self, url, content_type, title=None, thumb=None,
                   current_time=0, autoplay=True,
                   stream_type=STREAM_TYPE_BUFFERED,
                   metadata=None, subtitles=None, subtitles_lang='en-US',
                   subtitles_mime='text/vtt', subtitle_id=1):
        """
        Plays media on the Chromecast. Start default media receiver if not already started.
        Parameters:
        url: str - url of the media.
        content_type: str - mime type. Example: 'video/mp4'.
        title: str - title of the media.
        thumb: str - thumbnail image url.
        current_time: float - seconds from the beginning of the media to start playback.
        autoplay: bool - whether the media will automatically play.
        stream_type: str - describes the type of media artifact as one of the following: "NONE", "BUFFERED", "LIVE".
        subtitles: str - url of subtitle file to be shown on chromecast.
        subtitles_lang: str - language for subtitles.
        subtitles_mime: str - mimetype of subtitles.
        subtitle_id: int - id of subtitle to be loaded.
        metadata: dict - media metadata object, one of the following:
            GenericMediaMetadata, MovieMediaMetadata, TvShowMediaMetadata,
            MusicTrackMediaMetadata, PhotoMediaMetadata.
        Docs:
        https://developers.google.com/cast/docs/reference/messages#MediaData
        """
```

Objet MusicTrackMediaMetadata :

| Name         | Type              | Description                                                                                                                                                                                     |
| ------------ | ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| metadataType | integer           | 3  (the only value)                                                                                                                                                                             |
| albumName    | string            | optional Album or collection from which this track is drawn. Player can independently retrieve albumName using content_id or it can be given by the sender in the Load message                  |
| title        | string            | optional Name of the track (for example, song title). Player can independently retrieve title using content_id or it can be given by the sender in the Load message                             |
| albumArtist  | string            | optional Name of the artist associated with the album featuring this track. Player can independently retrieve albumArtist using content_id or it can be given by the sender in the Load message |
| artist       | string            | optional Name of the artist associated with the media track. Player can independently retrieve artist using content_id or it can be given by the sender in the Load message                     |
| composer     | string            | optional Name of the composer associated with the media track. Player can independently retrieve composer using content_id or it can be given by the sender in the Load message                 |
| trackNumber  | integer           | optional Number of the track on the album                                                                                                                                                       |
| discNumber   | integer           | optional Number of the volume (for example, a disc) of the album                                                                                                                                |
| images       | Image[]           | optional Array of URL(s) to an image associated with the content. The initial value of the field can be provided by the sender in the Load message. Should provide recommended sizes            |
| releaseDate  | string (ISO 8601) | optional ISO 8601 date and time this content was released. Player can independently retrieve releaseDate using content_id or it can be given by the sender in the Load message                  |

## Ecriture de DIALOGFLOW

- API version: 2

### Intents

- Name: DLNA_TEST
- Events: Welcome
- Training phrases:

#### *playlist*
  
| Parameter name | Entity   | Resolved value |
| -------------- | -------- | -------------- |
| playlist       | @sys.any | playlist       |

#### *playlist sur appareil*

| Parameter name | Entity    | Resolved value |
| -------------- | --------- | -------------- |
| playlist       | @sys.any  | playlist       |
| appareil       | @appareil | appareil       |

- Action and parameters: lance_playlist

| Required | Parameter name | Entity    | Value     | Is List | Prompts             |
| -------- | -------------- | --------- | --------- | ------- | ------------------- |
| X        | playlist       | @sys.any  | $playlist | X       | Je n'ai pas compris |
|          | appareil       | @appareil | $appareil |         |                     |

- Responses: Default

Text response 1: je joue la liste de lecture $playlist

- Fulfillment:

- [x] Enable webhook call for this intents

### Entities

rien

### Fulfillment

- Webhook: Enabled

| Name       | Description                               |                  |
| ---------- | ----------------------------------------- | ---------------- |
| URL*       | <https://myhome.googlehome.info/webhook/> |
| Basic Auth |                                           |                  |
| Headers    | token                                     | xxxxxxxxxxxxxxxx |
| Domains    | Disable webhook for all domains           |

[^1]: [Let's encrypt et Apache sous Raspbian](http://senufo2.blogspot.com/2018/12/lets-encrypt-et-apache-sous-raspbian.html)